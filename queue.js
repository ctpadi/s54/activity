let collection = [];


// Write the queue functions below.
function getArrayLength(arr){
	let count = 0;
	while(arr[count] !== undefined){
		count++;
	}
	return count;
}

// 1. Output all the elements of the queue
function print(){
	return collection;
}

// 2. Adds element to the rear of the queue
function enqueue(element){
	collection[getArrayLength(collection)] = element;
	return collection;
}

// 3. Removes element from the front of the queue
function dequeue(){
	let newCollection = [];
	for (let i = 1; i<getArrayLength(collection); i++){
		newCollection[i-1] = collection[i];
	}
	collection = newCollection;
	return collection;
}

// 4. Show element at the front
function front(){
	return collection[0];
}


// 5. Show the total number of elements
function size(){
	return getArrayLength(collection);
}



// 6. Outputs a Boolean value describing whether queue is empty or not
function isEmpty(){
	return getArrayLength(collection) === 0? true: false;
}



module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};